﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace backtracking_num15
{
    public partial class frmMain : Form
    {
        Graphics g;
        int Radius = 10; // радиус точки для ее прорисовки
        Points points; // все точки 
        SolidBrush blackBrush, redBrush;
        Points bestSubset, subset;  // множество точек, удовлетворяющих условию задачи
        int minDistance; // кратчайшее расстояние между центром тяжести искомого множества и началом координат 
        bool solved;

        Point begin; // начало координат

        public frmMain()
        {
            InitializeComponent();
            Application.Idle += new EventHandler(MainIdle);
        }

        private void MainIdle(object sender, EventArgs e)
        {
            btnStart.Enabled = points.Count > 0;
            btnClear.Enabled = btnStart.Enabled;
            DrawAxes();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            points = new Points();
            bestSubset = new Points();
            subset = new Points();
            minDistance = Int32.MaxValue;
            g = pbPlot.CreateGraphics();
            blackBrush = new SolidBrush(Color.Black);
            redBrush = new SolidBrush(Color.Red);
            begin = new Point(pbPlot.Width / 2, pbPlot.Height / 2);

            //DrawAxes();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!solved)
            {
                Task();
                DrawPoint(bestSubset.CenterOfGravity, redBrush, 5);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            points.Clear();
            subset.Clear();
            bestSubset.Clear();
            solved = false;
            minDistance = Int32.MaxValue;
            pbPlot.Refresh();
            DrawAxes();
        }

        private void numN_ValueChanged(object sender, EventArgs e)
        {
            pbPlot.Refresh();
            solved = false;
            //DrawAxes();
            DrawPoints(points, blackBrush);
        }

        private void pbPlot_MouseDown(object sender, MouseEventArgs e)
        {
            pbPlot.Refresh();
            //DrawAxes();
            DrawPoints(points, blackBrush);
            foreach (Point pt in points)
                if (GetDistance(pt, e.Location) < Radius + 3)
                    return;
            if (points.Add(e.Location))
                DrawPoint(e.Location, blackBrush, Radius);
            solved = false;
            subset.Clear();
            bestSubset.Clear();
        }

        private void pbPlot_Paint(object sender, PaintEventArgs e)
        {
            DrawAxes();
            DrawPoints(points, blackBrush);
            if (solved)
                DrawPoints(bestSubset, redBrush);

            /*if (!solved)
                DrawPoints(points, blackBrush);
            else
                DrawPoint(bestSubset.CenterOfGravity, redBrush, Radius + 6);*/
        }

        void Task()
        {
            subset.Clear();
            bestSubset.Clear();
            minDistance = Int32.MaxValue;
            solved = false;
            if (points.Count > numN.Value - 1)
            {
                solved = true;
                GetSolve((int)numN.Value, 0);
                DrawPoints(bestSubset, redBrush);
            }
            else
                MessageBox.Show("На плоскости точек находится меньше, чем " + numN.Value.ToString());
        }

        private void GetSolve(int N, int pos)
        {
            subset.Add(points[pos]);

            if (subset.Count == N)
                CheckOptimal();
            else
                if (pos < points.Count - 1)
                    GetSolve(N, pos + 1);                    

            subset.Delete(points[pos]);

            if (pos < points.Count - 1)
                GetSolve(N, pos + 1);
        }
        // проверка оптимальности текущего решения
        void CheckOptimal()
        {
            int dist = GetDistance(subset.CenterOfGravity, begin);
            //DrawPoint(subset.CenterOfGravity, redBrush, 5);
            if (dist < minDistance)
            {
                subset.Copy(bestSubset);
                minDistance = dist;
            }
        }

        // отрисова одной точки
        void DrawPoint(Point pt, Brush brush, int radius)
        {
            g.FillEllipse(brush, new Rectangle(new Point(pt.X - radius / 2, pt.Y - radius / 2), new Size(radius, radius)));
        }
        // отрисовка всех точек множества
        void DrawPoints(Points set, Brush brush)
        {
            foreach (Point pt in set)
                DrawPoint(pt, brush, Radius);
        }
        // отрисовка осей координат
        void DrawAxes()
        {
            Pen pen = new Pen(blackBrush);
            g.DrawLine(pen, 0, pbPlot.Height / 2, pbPlot.Width, pbPlot.Height / 2);
            g.DrawLine(pen, pbPlot.Width / 2, 0, pbPlot.Width / 2, pbPlot.Height);
        }
        // рассчет расстояния между двумя точками
        int GetDistance(Point p1, Point p2)
        {
            return (int)Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
    }
}
