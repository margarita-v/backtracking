﻿using System.Collections.Generic;
using System.Drawing;
using System.Collections;

namespace backtracking_num15
{
    class Points : IEnumerable
    {
        List<Point> points;
        bool gravCounted;
        Point centerOfGravity;

        // подсчет центра тяжести
        protected Point CalculateCenterOfGravity()
        {
            Point Res = new Point(0, 0);
            if (points.Count == 0)
                return Res;
            foreach (Point pt in points)
            {
                Res.X += pt.X;
                Res.Y += pt.Y;
            }
            Res.X /= points.Count;
            Res.Y /= points.Count;
            gravCounted = true;
            return Res;
        }

        public Points()
        {
            points = new List<Point>();
            gravCounted = false;
        }

        public bool Add(Point pt)
        {
            if (Contains(pt))
                return false;
            points.Add(pt);
            gravCounted = false;
            return true;
        }

        public bool Delete(Point pt)
        {
            if (!Contains(pt))
                return false;
            points.Remove(pt);
            gravCounted = false;
            return true;
        }

        public void Clear()
        {
            points.Clear();
            gravCounted = false;
        }

        // проверка элемента на принадлежность множеству
        public bool Contains(Point pt)
        {
            return points.Contains(pt);
        }

        public Point CenterOfGravity
        {
            get
            {
                if (!gravCounted)
                    centerOfGravity = CalculateCenterOfGravity();
                return centerOfGravity;
            }
        }

        // копирование всех элементов в передаваемое множество
        public void Copy(Points set)
        {
            set.Clear();
            for (int i = 0; i < points.Count; ++i)
                set.Add(points[i]);
        }

        public IEnumerator GetEnumerator()
        {
            return points.GetEnumerator();
        }

        public int Count
        {
            get { return points.Count; }
        }

        public Point this[int i]
        {
            get { return points[i]; }
            set { points[i] = value; }
        }
    }
}
